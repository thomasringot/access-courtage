const taildwindConfig = {
  theme: {
    colors: {
      blue: {
        default: '#4DC2F1',
        dark: '#2B9FCE',
      },
      brown: {
        default: '#2E1011',
        dark: '#1B0808',
      },
      black: '#000',
      white: '#FFF',
      gray: {
        light: '#FAFAFA',
        default: '#E5E5E5',
      },
      // testing colors
      red: 'red',
      green: 'green',
      pink: 'pink',
      purple: 'purple',
    },
    screens: {
      tablet: '768px',
      content: '940px',
      laptop: '1024px',
      desktop: '1440px',
    },
    extend: {},
  },
  variants: {},
  plugins: [],
};

module.exports = taildwindConfig;
