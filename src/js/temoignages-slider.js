import Glide from '@glidejs/glide';

document.addEventListener('DOMContentLoaded', function() {
  const temoignagesSlideControls = [
    ...document.querySelectorAll('.ac-slider--temoignages .js-slider-control'),
  ];
  const temoignagesSlider = new Glide('.glide.ac-slider--temoignages', {
    type: 'carousel',
  });

  temoignagesSlideControls.forEach(el => {
    el.addEventListener('click', e => {
      temoignagesSlider.go(e.target.textContent);
    });
  });
  temoignagesSlider.mount();
});
