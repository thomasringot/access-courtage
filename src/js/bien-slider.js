import Glide from '@glidejs/glide';

document.addEventListener('DOMContentLoaded', function() {
  const bienSlideControls = [
    ...document.querySelectorAll('.ac-slider--bien .js-slider-control'),
  ];
  const bienSlider = new Glide('.glide.ac-slider--bien', {
    type: 'carousel',
  });

  bienSlideControls.forEach(el => {
    el.addEventListener('click', e => {
      bienSlider.go(e.target.textContent);
    });
  });
  bienSlider.mount();
});
