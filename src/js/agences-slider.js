import Glide from '@glidejs/glide';

document.addEventListener('DOMContentLoaded', function() {
  const agencesSlideControls = [
    ...document.querySelectorAll('.ac-slider--agences .js-slider-control'),
  ];
  const agenceSlider = new Glide('.glide.ac-slider--agences', {
    perView: 3,
    type: 'carousel',
    breakpoints: {
      768: { perView: 1 },
    },
  });

  agencesSlideControls.forEach(el => {
    el.addEventListener('click', e => {
      agenceSlider.go(e.target.textContent);
    });
  });
  agenceSlider.mount();
});
