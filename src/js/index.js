document.addEventListener('DOMContentLoaded', function() {
  // SETUP
  const $ = {
    header: document.querySelector('.js-header'),
    toggleMenu: document.querySelector('.js-toggle-menu'),
  };

  // TOGGLE MENU FEATURE
  $.toggleMenu.addEventListener('click', function() {
    document.body.classList.toggle('overflow-hidden');
    $.header.classList.toggle('is-active');
    $.toggleMenu.classList.toggle('is-active');
  });
});
